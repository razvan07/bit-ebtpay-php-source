<?php

require_once 'vendor/autoload.php';
session_start();
ob_start();

use App\Helpers\Initialize;
?>
<!doctype html>
<html lang="en">
    <head>
        <?php require "app/Resources/Public/Partials/HeaderResources.php"; ?>
    </head>
    <body class="container">
        <?php

         require "app/Resources/Public/Partials/Header.php";

         $init = new Initialize();
         $init->initialize();

         require "app/Resources/Public/Partials/FooterResources.php";

        ?>
    </body>
</html>
<?php ob_end_flush(); ?>
