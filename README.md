Pitech eBTPay
================

Install the project on your local env

1 - Clone the project:
```
git clone git@gitlab.pitechplus.com:payment/ebtpay-php-source.git && cd ebtpay-php-source
```

2 - Use composer to install the project
```
composer require payment/ebtpay-php-source:1.0.0
```
