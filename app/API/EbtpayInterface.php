<?php

namespace App\API;

/**
 * Interface EbtpayInterface
 *
 * @package App\API
 */
interface EbtpayInterface
{
    /**
     * @param array $params
     *
     * @return mixed
     */
    public function setRegister(array $params);

    /**
     * @param array $params
     *
     * @return mixed
     */
    public function setPreAuthRegister(array $params);

    /**
     * @param array $params
     *
     * @return mixed
     */
    public function setDeposit(array $params);

    /**
     * @param string $param
     *
     * @return mixed
     */
    public function setReverse(string $param);

    /**
     * @param array $param
     *
     * @return mixed
     */
    public function setRefund(array $param);

    /**
     * @param string $param
     *
     * @return mixed
     */
    public function getOrderStatus(string $param);

    /**
     * @return mixed
     */
    public function getOrderStatusExtended();

    /**
     * @return mixed
     */
    public function setPaymentToOrder();

    /**
     * @return mixed
     */
    public function getFinis3dsPayment();

    /**
     * @return mixed
     */
    public function verify3dsEnrollment();

    /**
     * @return mixed
     */
    public function setRecurringPayment();

    /**
     * @return mixed
     */
    public function getLastOrdersForMerchants();
}
