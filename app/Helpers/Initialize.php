<?php

namespace App\Helpers;

use Dotenv\Dotenv;

/**
 * Class Initialize
 *
 * @package App\Helpers
 */
Class Initialize
{
    /** @var Http */
    protected $http;

    /**
     * Initialize constructor.
     */
    public function __construct()
    {
        $this->http = new Http();
    }

    /**
     *  Initialize the app
     *
     * @throws \Exception
     */
    public function initialize()
    {
        if (array_key_exists('request', $this->http->getQuery())) {
            $query = $this->http->getQuery('request');
            $temporary = explode('/', $query);

            if (array_key_exists(0, $temporary)) {
                $this->http->setQuery('C', $temporary[0]);
            }

            if (array_key_exists(1, $temporary)) {
                $this->http->setQuery('A', $temporary[1]);
            }

            unset($temporary);
            unset($query);
        }

        if (array_key_exists('C', $this->http->getQuery())) {
            $controller = $this->http->getQuery('C') . 'Controller';
        } else {
            $controller = 'DefaultController';
        }
        if (array_key_exists('A', $this->http->getQuery())) {
            $action = $this->http->getQuery('A') . 'Action';
        } else {
            $action = 'defaultAction';
        }

        $controller = 'App\Controllers\\' . $controller;

        $this->loadEnv();

        $obj = new $controller();
        $obj->$action();
    }

    /**
     * Load the data from .env
     */
    private function loadEnv()
    {
        $dotenv = Dotenv::create(Http::getServer('DOCUMENT_ROOT'));
        $dotenv->overload();
    }
}
