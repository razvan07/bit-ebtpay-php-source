<?php

namespace App\Helpers;

/**
 * Class UrlBuilder
 *
 * @package App\Helper
 */
Class UrlBuilder
{
    /**
     * @param $controller
     * @param string $action
     *
     * @return null|string
     */
    public static function create($controller, $action = 'default')
    {
        $path = '';

        if (Http::getEnv('ABSOLUTE_PATH')) {
            $path = Http::getEnv('BASE_URL');
        }

        if (Http::getEnv('REWRITE_MODE')) {
            $path .= $controller . '/' . $action;
        } else {
            $path .= 'index.php?C=' . $controller . '&A=' . $action;
        }

        return $path;
    }

    /**
     * @param $controller
     * @param string $action
     */
    public static function redirect($controller, $action = 'default')
    {
        if (!strpos($controller, '//')) {
            $path = self::create($controller, $action);
        } else {
            $path = $controller;
        }

        echo "<meta http-equiv='refresh' content='0; url=" . $path . "' />";
    }
}
