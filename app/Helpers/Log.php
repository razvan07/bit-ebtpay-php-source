<?php

namespace App\Helpers;

/**
 * Class Log
 *
 * @package App\Helpers
 */
class Log
{
    /**
     * @param null $message
     */
    public static function log($message = null)
    {
        if (!$message) {
            return;
        }

        $file = fopen(Http::getServer('DOCUMENT_ROOT') . '/system.log', 'a');
        fwrite($file, "\n" . '[' . date('Y-m-d h:i:s') . ']' . ' :: ' . $message);
        fclose($file);
    }
}
