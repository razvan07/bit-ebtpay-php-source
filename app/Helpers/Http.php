<?php

namespace App\Helpers;

/**
 * Class Http
 *
 * @package App\Helpers
 */
class Http
{
    /** Scheme for http */
    const SCHEME_HTTP = 'http';

    /** Scheme for https */
    const SCHEME_HTTPS = 'https';

    /**
     * Retrieve a member of the $_SESSION superglobal
     *
     * If no $key is passed, returns the entire $_SESSION array.
     *
     * @param string $key
     * @param mixed $default Default value to use if key not found
     *
     * @return mixed Returns null if key does not exist
     */
    public static function getSession($key = null, $default = null)
    {
        if (!$key) {
            return $_SESSION;
        }

        return (isset($_SESSION[$key])) ? $_SESSION[$key] : $default;
    }

    /**
     * Unset Session
     *
     * @param string $key
     */
    public static function unsSession(string $key)
    {
        if (isset($_SESSION[$key])) {
            unset($_SESSION[$key]);
        }
    }

    /**
     * Set Session
     *
     * @param $spec
     * @param null $value
     *
     * @return $this
     *
     * @throws \Exception
     */
    public function setSession($spec, $value = null)
    {
        if (!$value && !is_array($spec)) {
            throw new \Exception(
                'Invalid value passed to setSession(); must be either array of values or key/value pair'
            );
        }

        if (!$value && is_array($spec)) {
            foreach ($spec as $key => $value) {
                $this->setSession($key, $value);
            }

            return $this;
        }

        $_SESSION[(string) $spec] = $value;

        return $this;
    }

    /**
     * Retrieve a member of the $_POST superglobal
     *
     * If no $key is passed, returns the entire $_POST array.
     *
     * @param string $key
     * @param mixed $default Default value to use if key not found
     *
     * @return mixed Returns null if key does not exist
     */
    public function getPost($key = null, $default = null)
    {
        if (!$key) {
            return $_POST;
        }

        return (isset($_POST[$key])) ? $_POST[$key] : $default;
    }

    /**
     * Set POST values
     *
     * @param  string|array $spec
     * @param  null|mixed $value
     *
     * @return Http
     *
     * @throws \Exception
     */
    public function setPost($spec, $value = null)
    {
        if (!$value && !is_array($spec)) {
            throw new \Exception('Invalid value passed to setPost(); must be either array of values or key/value pair');
        }

        if (!$value && is_array($spec)) {
            foreach ($spec as $key => $value) {
                $this->setPost($key, $value);
            }

            return $this;
        }

        $_POST[(string) $spec] = $value;

        return $this;
    }

    /**
     * Retrieve a member of the $_SERVER superglobal
     *
     * If no $key is passed, returns the entire $_SERVER array.
     *
     * @param string $key
     * @param mixed $default Default value to use if key not found
     *
     * @return mixed Returns null if key does not exist
     */
    public static function getServer($key = null, $default = null)
    {
        if (!$key) {
            return $_SERVER;
        }

        return (isset($_SERVER[$key])) ? $_SERVER[$key] : $default;
    }

    /**
     * Retrieve a member of the $_ENV superglobal
     *
     * If no $key is passed, returns the entire $_ENV array.
     *
     * @param string $key
     * @param mixed $default Default value to use if key not found
     *
     * @return mixed Returns null if key does not exist
     */
    public static function getEnv($key = null, $default = null)
    {
        if (!$key) {
            return $_ENV;
        }

        return (isset($_ENV[$key])) ? $_ENV[$key] : $default;
    }

    /**
     * Retrieve a member of the $_GET superglobal
     *
     * If no $key is passed, returns the entire $_GET array.
     *
     * @param string $key
     * @param mixed $default Default value to use if key not found
     *
     * @return mixed Returns null if key does not exist
     */
    public function getQuery($key = null, $default = null)
    {
        if (!$key) {
            return $_GET;
        }

        return (isset($_GET[$key])) ? $_GET[$key] : $default;
    }

    /**
     * Set GET values
     *
     * @param  string|array $spec
     * @param  null|mixed $value
     *
     * @return Http
     *
     * @throws \Exception
     */
    public function setQuery($spec, $value = null)
    {
        if (!$value && !is_array($spec)) {
            throw new \Exception(
                'Invalid value passed to setQuery(); must be either array of values or key/value pair'
            );
        }

        if (!$value && is_array($spec)) {
            foreach ($spec as $key => $value) {
                $this->setQuery($key, $value);
            }

            return $this;
        }

        $_GET[(string) $spec] = $value;

        return $this;
    }

    /**
     * Return the method by which the request was made
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->getServer('REQUEST_METHOD');
    }

    /**
     * Was the request made by POST?
     *
     * @return boolean
     */
    public function isPost()
    {
        if ('POST' == $this->getMethod()) {
            return true;
        }

        return false;
    }

    /**
     * Was the request made by GET?
     *
     * @return boolean
     */
    public function isGet()
    {
        if ('GET' == $this->getMethod()) {
            return true;
        }

        return false;
    }

    /**
     * Was the request made by PUT?
     *
     * @return boolean
     */
    public function isPut()
    {
        if ('PUT' == $this->getMethod()) {
            return true;
        }

        return false;
    }

    /**
     * Was the request made by DELETE?
     *
     * @return boolean
     */
    public function isDelete()
    {
        if ('DELETE' == $this->getMethod()) {
            return true;
        }

        return false;
    }

    /**
     * Get the request URI scheme
     *
     * @return string
     */
    public function getScheme()
    {
        return ($this->getServer('HTTPS') == 'on') ? self::SCHEME_HTTPS : self::SCHEME_HTTP;
    }

    /**
     * Get the HTTP host.
     *
     * "Host" ":" host [ ":" port ] ;
     * Note the HTTP Host header is not the same as the URI host.
     * It includes the port while the URI host doesn't.
     *
     * @return string
     */
    public function getHttpHost()
    {
        $host = $this->getServer('HTTP_HOST');
        if (!empty($host)) {
            return $host;
        }

        $scheme = $this->getScheme();
        $name = $this->getServer('SERVER_NAME');
        $port = $this->getServer('SERVER_PORT');

        if (!$name) {
            return '';
        } elseif (($scheme == self::SCHEME_HTTP && $port == 80) || ($scheme == self::SCHEME_HTTPS && $port == 443)) {
            return $name;
        } else {
            return $name . ':' . $port;
        }
    }

    /**
     * Get the client's IP addres
     *
     * @param  boolean $checkProxy
     *
     * @return string
     */
    public function getClientIp($checkProxy = true)
    {
        if ($checkProxy && $this->getServer('HTTP_CLIENT_IP') != null) {
            $ip = $this->getServer('HTTP_CLIENT_IP');
        } else {
            if ($checkProxy && $this->getServer('HTTP_X_FORWARDED_FOR') != null) {
                $ip = $this->getServer('HTTP_X_FORWARDED_FOR');
            } else {
                $ip = $this->getServer('REMOTE_ADDR');
            }
        }

        return $ip;
    }
}
