<?php

namespace App\Views;

/**
 * Class View
 */
class View
{
    /** @var array */
    protected $variables = [];

    /**
     * @param $name
     * @param $value
     */
    public function set($name, $value)
    {
        $variableNames = ['controller', 'action'];

        if (!in_array($name, $variableNames)) {
            $this->variables[$name] = $value;
        }
    }

    /**
     * @param $method
     */
    public function render($method)
    {
        $method = explode('::', $method);
        $controller = explode('\\', $method[0]);

        $controller = preg_replace('/Controller$/', '', $controller[2] ?? '');
        $method = preg_replace('/Action$/', '', $method[1] ?? '');

        foreach ($this->variables as $key => $value) {
            $$key = $value;
        }

        require __DIR__ . '/' . $controller . '/' . $method . '.php';
    }
}
