<?php use App\Helpers\UrlBuilder; ?>
<section class="new-order">
    <div class="row">
        <div class="col-md-12 text-center">
            <h3>Create Order</h3>
        </div>
        <div class="col-md-6 offset-md-3">
            <form method="POST" action='<?php echo UrlBuilder::create('Orders', 'create'); ?>'>
                <div class="form-group">
                    <label for="orderNumber">Order Number</label>
                    <input type="text" name="orderNumber" class="form-control" id="orderNumber" aria-describedby="orderNumber" placeholder="Order Number" required>
                    <small id="orderNumber" class="form-text text-muted">Number (identifier) of the order in the merchant's system</small>
                </div>
                <div class="form-group">
                    <label for="amount">Amount</label>
                    <input type="number" name="amount" class="form-control" id="amount" placeholder="Amount" required>
                    <small id="orderNumber" class="form-text text-muted">Order amount in minimal currency units</small>
                </div>
                <div class="form-group">
                    <label for="currency">Currency</label>
                    <input type="number" name="currency" class="form-control" id="currency" placeholder="Currency" required>
                    <small id="orderNumber" class="form-text text-muted">Code number of the payment currency, according to ISO 4217 (RON 946 & EUR 978)</small>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="paymentPhase" id="onePhase" value="onePhase" required>
                    <label class="form-check-label" for="onePhase">
                        One-phase payment request
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="paymentPhase" id="twoPhase" value="twoPhase" checked required>
                    <label class="form-check-label" for="twoPhase">
                        Two-phase payment request
                    </label>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</section>
