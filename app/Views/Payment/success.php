<?php

use App\Helpers\UrlBuilder;
use App\Helpers\Http;

$paymentPhase = Http::getSession('paymentPhase') ?? false;
$isCaptured = false;
$isRefund = false;
$isReverse = false;

switch ($status['OrderStatus'] ?? '') {
    case 1:
        $statusOrder = 'Pre-authorization amount was held (for two-phase payment)';
        break;
    case 2:
        $statusOrder = 'The amount was deposited successfully';
        $isCaptured = true;
        break;
    case 3:
        $statusOrder = 'Authorization reversed';
        $isRefund = true;
        break;
    case 4:
        $statusOrder = 'Transaction was refunded.';
        $isReverse = true;
        break;
    case 5:
        $statusOrder = 'Authorization through the issuer&#39;s ACS initiated';
        break;
}
?>
<section class="success-payment">
    <div class="row top">
        <?php if ($paymentPhase): ?>
            <div class="col-md-12 text-center alert alert-success" role="alert">
                <h3><?php echo $statusOrder ?? 'No message' ?></p></h3>
            </div>
        <?php else: ?>
            <h3>Select your new action</h3>
        <?php endif; ?>
    </div>
    <div class="row content">
        <div class="col-md-12 text-center">
            <p>Cardholder Name: <?php echo $status['cardholderName'] ?? '' ?></p>
            <p>Card Number: <?php echo $status['Pan'] ?? '' ?></p>
            <p>Amount: <?php echo $status['Amount'] ?? '' ?></p>
            <p>Deposit Amount: <?php echo $status['depositAmount'] ?? '' ?></p>
            <p class="green">Order Status: <?php echo $statusOrder ?? '' ?></p>
        </div>
    </div>
    <div class="row buttons">
        <div class="col-md-12 text-center">
            <?php if (!$isCaptured && !$isRefund && !$isReverse): ?>
            <button type="button" class="btn btn-primary btn-lg"
                    onclick="location.href='<?php echo UrlBuilder::create('Payment', 'reversal'); ?>';">Reversal
            </button>
            <?php endif; ?>
            <?php if ($isCaptured && !$isRefund && !$isReverse): ?>
            <button type="button" class="btn btn-primary btn-lg"
                    onclick="location.href='<?php echo UrlBuilder::create('Payment', 'refund'); ?>';">Refund
            </button>
            <?php endif; ?>
            <?php if ($paymentPhase == 'twoPhase' && !$isCaptured && !$isReverse && !$isRefund): ?>
                <button type="button" class="btn btn-primary btn-lg"
                        onclick="location.href='<?php echo UrlBuilder::create('Payment', 'capture'); ?>';">Capture
                </button>
            <?php endif; ?>
            <button type="button" class="btn btn-primary btn-lg"
                    onclick="location.href='<?php echo UrlBuilder::create('Default', 'default'); ?>';">Back
            </button>
        </div>
    </div>
</section>
