<?php

use App\Helpers\UrlBuilder;
use App\Helpers\Http;

$paymentPhase = Http::getSession('paymentPhase') ?? false;

switch ($status['OrderStatus'] ?? '') {
    case 0:
        $statusOrder = 'Order registered, but not paid off.';
        break;
    case 6:
        $statusOrder = 'Authorization declined';
        break;
}
?>
<section class="failed-payment">
    <div class="row top">
        <?php if ($paymentPhase): ?>
            <div class="col-md-12 text-center alert alert-danger" role="alert">
                <h3><?php echo $statusOrder ?? 'No message' ?></p></h3>
            </div>
        <?php else: ?>
            <div class="col-md-12 text-center alert alert-danger" role="alert">
                <h3>Something went wrong</h3>
            </div>
        <?php endif; ?>
    </div>
    <div class="row content">
        <div class="col-md-12 text-center">
            <p>Cardholder Name: <?php echo $status['cardholderName'] ?? '' ?></p>
            <p>Card Number: <?php echo $status['Pan'] ?? '' ?></p>
            <p>Amount: <?php echo $status['Amount'] ?? '' ?></p>
            <p>Deposit Amount: <?php echo $status['depositAmount'] ?? '' ?></p>
            <p class="red">Order Status: <?php echo $statusOrder ?? '' ?></p>
        </div>
    </div>
    <div class="row buttons">
        <div class="col-md-12 text-center">
            <button type="button" class="btn btn-primary btn-lg"
                    onclick="location.href='<?php echo UrlBuilder::create('Default', 'default'); ?>';">Back
            </button>
        </div>
    </div>
</section>
