<?php

namespace App\Models\Api;

use Curl\Curl;

/**
 * Class AbstractTransport
 *
 * @package App\Models\Api
 */
abstract class AbstractTransport
{
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';

    /** @var Curl */
    protected $curl;

    /**
     * AbstractTransport constructor.
     *
     * @throws \ErrorException
     */
    public function __construct()
    {
        $this->curl = new Curl();
    }

    /**
     * Build the API request and send it
     *
     * @param $url
     * @param $method
     * @param array $params
     *
     * @return null
     *
     * @throws \Exception
     */
    protected function doRequest($url, $method, $params = [])
    {
        $url = trim($url);
        $callMethod = strtolower($method);

        if ($method === self::METHOD_GET) {
            $url = $this->addRequestParameter($url, $params);
        }

        try {
            $this->curl->{$callMethod}($url, $params);
        } catch (\Exception $exception) {
            throw new \Exception('Message ' . $exception->getMessage() . '; Code ' . $exception->getCode());
        }

        return $this->curl->getResponse();
    }

    /**
     * @param $url
     * @param $params
     *
     * @return string
     */
    protected function addRequestParameter($url, $params)
    {
        if (is_array($params) && !empty($params)) {
            $url .= '?' . http_build_query($params, '', '&');
        }

        return $url;
    }

    /**
     * Set curl header
     */
    protected function setHeader()
    {
        $this->curl->setHeader('Content-Type', 'application/x-www-form-urlencoded');
    }
}
