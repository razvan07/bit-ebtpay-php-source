<?php

namespace App\Models\Api\Ebtpay;

use App\Models\Api\AbstractTransport;
use App\Helpers\Http;
use App\Helpers\UrlBuilder;
use App\Helpers\Log;

/**
 * Class Transport
 *
 * @package App\Models\Api\Ebtpay
 */
class Transport extends AbstractTransport
{
    /**
     * @param array $params
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function setPreAuthRegister(array $params)
    {
        $url = $this->getApiBaseUrl() . 'registerPreAuth.do';
        unset($params['paymentPhase']);
        $this->setHeader();
        $description = ['description' => 'Preauthorized order SandBox Pitech'];
        $body = array_merge($this->getApiAuthentication(), $this->getReturningUrl(), $params, $description);
        Log::log('PreAuthRegister parameter: ' . json_encode($body));

        return $this->doRequest($url, self::METHOD_POST, $body);
    }

    /**
     * @param array $params
     *
     * @return null
     *
     * @throws \Exception
     */
    public function setRegister(array $params)
    {
        $url = $this->getApiBaseUrl() . 'register.do';
        unset($params['paymentPhase']);
        $this->setHeader();
        $description = ['description' => 'Register order SandBox Pitech'];
        $body = array_merge($this->getApiAuthentication(), $this->getReturningUrl(), $params, $description);
        Log::log('Register parameter: ' . json_encode($body));

        return $this->doRequest($url, self::METHOD_POST, $body);
    }

    /**
     * @param string $param
     * @return null
     *
     * @throws \Exception
     */
    public function getOrderStatus(string $param)
    {
        $url = $this->getApiBaseUrl() . 'getOrderStatus.do';
        $this->setHeader();
        $body = array_merge($this->getApiAuthentication(), ['orderId' => $param]);
        Log::log('GetOrderStatus parameter: ' . json_encode($body));

        return $this->doRequest($url, self::METHOD_POST, $body);
    }

    /**
     * @param array $params
     *
     * @return null
     *
     * @throws \Exception
     */
    public function setDeposit(array $params)
    {
        $url = $this->getApiBaseUrl() . 'deposit.do';
        $this->setHeader();
        $body = array_merge($this->getApiAuthentication(), $params);
        Log::log('Deposit parameter: ' . json_encode($body));

        return $this->doRequest($url, self::METHOD_POST, $body);
    }

    /**
     * @param string $param
     *
     * @return null
     *
     * @throws \Exception
     */
    public function setReverse(string $param)
    {
        $url = $this->getApiBaseUrl() . 'reverse.do';
        $this->setHeader();
        $body = array_merge($this->getApiAuthentication(), ['orderId' => $param]);
        Log::log('Reverse parameter: ' . json_encode($body));

        return $this->doRequest($url, self::METHOD_POST, $body);
    }

    /**
     * @param array $params
     *
     * @return null
     *
     * @throws \Exception
     */
    public function setRefund(array $params)
    {
        $url = $this->getApiBaseUrl() . 'refund.do';
        $this->setHeader();
        $body = array_merge($this->getApiAuthentication(), $params);
        Log::log('Refund parameter: ' . json_encode($body));

        return $this->doRequest($url, self::METHOD_POST, $body);
    }

    /**
     * @return array
     */
    private function getReturningUrl()
    {
        return [
            'returnUrl' => URLBuilder::create('Payment', 'success'),
            'failUrl' => URLBuilder::create('Payment', 'failed'),
        ];
    }

    private function getApiAuthentication()
    {
        return [
            'userName' => Http::getEnv('BT_API_USER'),
            'password' => Http::getEnv('BT_API_PASSWORD'),
        ];
    }

    /**
     * @return string
     */
    private function getApiBaseUrl()
    {
        return Http::getEnv('BT_API_URL');
    }

//
//
//    /**
//     * Call CW API to get the authentication token
//     *
//     * @param string $proposalId
//     * @return string
//     *
//     * @throws ApiException
//     */
//    public function getAuthenticationToken(string $proposalId)
//    {
//        $url = $this->getBaseUrl() . sprintf('/proposals/%s/auth/login', $proposalId);
//
//        $body = [
//            'UserToken' => null,
//            'Username' => null,
//            'Password' => null,
//            'ApiKey' => null,
//            'SystemKey' => null,
//            'Referrer' => null,
//            'AssociatedDealerKey' => null,
//            'RetailerIdentifier' => null,
//        ];
//
//        /** @var CoreCurl $curl */
//        $curl = $this->clientFactory->create();
//        $this->initCurl($curl);
//        $body = json_encode($body);
//
//        $this->logger->info('[CW FINAPP]: Authentication token || URL ||: ' . $url);
//        $this->logger->info('[CW FINAPP]: Authentication token || BODY ||: ' . $body);
//
//        return $this->doRequest($url, Request::METHOD_POST, $body);
//    }
}
