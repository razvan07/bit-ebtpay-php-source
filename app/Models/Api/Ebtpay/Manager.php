<?php

namespace App\Models\Api\Ebtpay;

use App\API\EbtpayInterface;
use App\Helpers\Http;
use App\Helpers\Log;

/**
 * Class Manager
 *
 * @package App\Models\Api\Ebtpay
 */
class Manager implements EbtpayInterface
{
    /** @var Transport */
    protected $transport;

    /** @var Http */
    protected $http;

    /**
     * Manager constructor.
     *
     * @throws \ErrorException
     */
    public function __construct()
    {
        $this->transport = new Transport();
        $this->http = new Http();
    }

    /**
     * Deposit the money without authorization
     *
     * @param array $params
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function setRegister(array $params)
    {
        $response = $this->transport->setRegister($params);
        $decodedResponse = json_decode($response, true);

        if (json_last_error() !== JSON_ERROR_NONE ||
            (isset($decodedResponse['errorCode']) && $decodedResponse['errorCode'] != 0)) {
            $this->logAndThrowError('Invalid response for Register: ' . json_encode($decodedResponse));
        } else {
            Log::log('Register response: ' . $response);
        }

        return $decodedResponse;
    }

    /**
     * Authorization of the deposit
     *
     * @param array $params
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function setPreAuthRegister(array $params)
    {
        $response = $this->transport->setPreAuthRegister($params);
        $decodedResponse = json_decode($response, true);

        if (json_last_error() !== JSON_ERROR_NONE ||
            (isset($decodedResponse['errorCode']) && $decodedResponse['errorCode'] != 0)) {
            $this->logAndThrowError('Invalid response for PreAuthRegister: ' . json_encode($decodedResponse));
        } else {
            Log::log('PreAuthRegister response: ' . $response);
        }

        return $decodedResponse;
    }

    /***
     * @param string $param
     * @return mixed
     *
     * @throws \Exception
     */
    public function getOrderStatus(string $param)
    {
        $response = $this->transport->getOrderStatus($param);
        $decodedResponse = json_decode($response, true);

        if (json_last_error() !== JSON_ERROR_NONE ||
            (isset($decodedResponse['errorCode']) && $decodedResponse['errorCode'] != 0)) {
            $this->logAndThrowError('Invalid response for GetOrderStatus: ' . json_encode($decodedResponse));
        } else {
            Log::log('GetOrderStatus response: ' . $response);
        }

        return $decodedResponse;
    }

    /**
     * @param array $params
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function setDeposit(array $params)
    {
        $response = $this->transport->setDeposit($params);
        $decodedResponse = json_decode($response, true);

        if (json_last_error() !== JSON_ERROR_NONE ||
            (isset($decodedResponse['errorCode']) && $decodedResponse['errorCode'] != 0)) {
            $this->logAndThrowError('Invalid response for Deposit: ' . json_encode($decodedResponse));
        } else {
            Log::log('Deposit response: ' . $response);
        }

        return $decodedResponse;
    }

    /**
     * @param string $param
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function setReverse(string $param)
    {
        $response = $this->transport->setReverse($param);
        $decodedResponse = json_decode($response, true);

        if (json_last_error() !== JSON_ERROR_NONE ||
            (isset($decodedResponse['errorCode']) && $decodedResponse['errorCode'] != 0)) {
            $this->logAndThrowError('Invalid response for Reverse: ' . json_encode($decodedResponse));
        } else {
            Log::log('Reverse response: ' . $response);
        }

        return $decodedResponse;
    }

    /**
     * @param array $params
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function setRefund(array $params)
    {
        $response = $this->transport->setRefund($params);
        $decodedResponse = json_decode($response, true);

        if (json_last_error() !== JSON_ERROR_NONE ||
            (isset($decodedResponse['errorCode']) && $decodedResponse['errorCode'] != 0)) {
            $this->logAndThrowError('Invalid response for Refund: ' . json_encode($decodedResponse));
        } else {
            Log::log('Refund response: ' . $response);
        }

        return $decodedResponse;
    }

    /**
     * @return mixed
     */
    public function getOrderStatusExtended()
    {
        // TODO: Implement getOrderStatusExtended() method.
    }

    /**
     * @return mixed
     */
    public function setPaymentToOrder()
    {
        // TODO: Implement setPaymentToOrder() method.
    }

    /**
     * @return mixed
     */
    public function getFinis3dsPayment()
    {
        // TODO: Implement getFinis3dsPayment() method.
    }

    /**
     * @return mixed
     */
    public function verify3dsEnrollment()
    {
        // TODO: Implement verify3dsEnrollment() method.
    }

    /**
     * @return mixed
     */
    public function setRecurringPayment()
    {
        // TODO: Implement setRecurringPayment() method.
    }

    /**
     * @return mixed
     */
    public function getLastOrdersForMerchants()
    {
        // TODO: Implement getLastOrdersForMerchants() method.
    }

    /**
     * @param $errorMessage
     *
     * @throws \Exception
     */
    private function logAndThrowError($errorMessage)
    {
        Log::log($errorMessage);
        throw new \Exception($errorMessage);
    }
}
