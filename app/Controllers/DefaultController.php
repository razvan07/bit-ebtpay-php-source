<?php

namespace App\Controllers;

/**
 * Class DefaultController
 *
 * @package App\Controllers
 */
Class DefaultController extends Controller
{
    /**
     * @return mixed|void
     */
    public function defaultAction()
    {
        $this->view->render(__METHOD__);
    }
}
