<?php

namespace App\Controllers;

use App\Models\Api\Ebtpay\Manager as TransportManager;
use App\Helpers\Http;
use App\Helpers\UrlBuilder;

/**
 * Class PaymentController
 *
 * @package App\Controllers
 */
class PaymentController extends Controller
{
    const FAILED_ORDER_STATUS = [0, 6];

    /** @var TransportManager */
    protected $transportManager;

    /** @var Http */
    protected $http;

    /** store order status */
    protected $orderStatus;

    /**
     * PaymentController constructor
     *
     * @throws \ErrorException
     */
    public function __construct()
    {
        $this->transportManager = new TransportManager();
        $this->http = new Http();
        parent::__construct();
    }

    /**
     * @return mixed|void
     */
    public function defaultAction()
    {
        UrlBuilder::redirect('Default');
    }

    /**
     * @throws \Exception
     */
    public function successAction()
    {
        if (Http::getSession('paymentPhase')) {
            $orderStatus = $this->getOrderStatus();

            if (in_array($orderStatus['OrderStatus'], self::FAILED_ORDER_STATUS)) {
                URLBuilder::redirect('Payment', 'failed');
            }

            $this->view->set('status', $orderStatus);
            $this->view->render(__METHOD__);
        } else {
            $this->failedAction();
        }
    }

    /**
     * @throws \Exception
     */
    public function failedAction()
    {
        $this->view->set('status', $this->getOrderStatus());
        $this->view->render(__METHOD__);
    }

    /**
     * @throws \Exception
     */
    public function captureAction()
    {
        $orderStatus = $this->getOrderStatus();
        $orderId = Http::getSession('orderId');

        if (isset($orderStatus['Amount']) && $orderId) {
            $this->transportManager->setDeposit(['amount' => $orderStatus['Amount'], 'orderId' => $orderId]);
            URLBuilder::redirect('Payment', 'success');
        } else {
            URLBuilder::redirect('Payment', 'failed');
        }
    }

    /**
     * @throws \Exception
     */
    public function reversalAction()
    {
        $orderId = Http::getSession('orderId');

        if ($orderId) {
            $this->transportManager->setReverse($orderId);
            URLBuilder::redirect('Payment', 'success');
        } else {
            URLBuilder::redirect('Payment', 'failed');
        }
    }

    /**
     * @throws \Exception
     */
    public function refundAction()
    {
        $orderStatus = $this->getOrderStatus();
        $orderId = Http::getSession('orderId');

        if (isset($orderStatus['Amount']) && $orderId) {
            $this->transportManager->setRefund(['amount' => $orderStatus['Amount'], 'orderId' => $orderId]);
            URLBuilder::redirect('Payment', 'success');
        } else {
            URLBuilder::redirect('Payment', 'failed');
        }
    }

    /**
     * @return mixed
     *
     * @throws \Exception
     */
    public function getOrderStatus()
    {
        if (!$this->orderStatus) {
            $this->orderStatus = $this->transportManager->getOrderStatus(Http::getSession('orderId'));
        }

        return $this->orderStatus;
    }
}
