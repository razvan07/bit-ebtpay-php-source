<?php

namespace App\Controllers;

use App\Models\Api\Ebtpay\Manager as TransportManager;
use App\Helpers\UrlBuilder;
use App\Helpers\Http;

/**
 * Class OrdersController
 *
 * @package App\Controllers
 */
Class OrdersController extends Controller
{
    const ONE_PHASE = 'onePhase';
    const TWO_PHASE = 'twoPhase';

    /** @var Http */
    protected $http;

    /** @var TransportManager */
    protected $transportManager;

    /**
     * OrdersController constructor.
     *
     * @throws \ErrorException
     */
    public function __construct()
    {
        $this->http = new Http();
        $this->transportManager = new TransportManager();
        parent::__construct();
    }

    /**
     * @return mixed|void
     */
    public function defaultAction()
    {
        UrlBuilder::redirect('Default');
    }

    /**
     * Create order in gateway
     *
     * @throws \Exception
     */
    public function createAction()
    {
        if ($this->http->isPost() && $this->http->getPost('paymentPhase')) {
            if ($this->http->getPost('paymentPhase') == self::ONE_PHASE) {
                $this->http->setSession('paymentPhase', self::ONE_PHASE);

                $response = $this->transportManager->setRegister($this->http->getPost());
                $this->http->setSession('orderId', $response['orderId'] ?? '');

                URLBuilder::redirect($response['formUrl'] ?? 'Default');
            } elseif ($this->http->getPost('paymentPhase') == self::TWO_PHASE) {
                $this->http->setSession('paymentPhase', self::TWO_PHASE);

                $response = $this->transportManager->setPreAuthRegister($this->http->getPost());
                $this->http->setSession('orderId', $response['orderId'] ?? '');

                URLBuilder::redirect($response['formUrl'] ?? 'Default');
            }
        } else {
            Http::unsSession('paymentPhase');
            $this->defaultAction();
        }
    }
}
