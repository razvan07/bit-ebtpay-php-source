<?php

namespace App\Controllers;

use App\Views\View;

/**
 * Class Controller
 *
 * @package App\Controllers
 */
abstract Class Controller
{
    /** @var View */
    protected $view;

    /**
     * Controller constructor
     */
    public function __construct()
    {
        $this->view = new View();
    }

    /**
     * @return mixed
     */
    abstract function defaultAction();
}
